
(defpackage :pcall-connection-manager-system
  (:use :cl))

(in-package :pcall-connection-manager-system)

(asdf:defsystem :pcall-connection-manager
  :depends-on (:hunchentoot :pcall :bordeaux-threads)
  :components
  ((:file "pcall-connection-manager")))
