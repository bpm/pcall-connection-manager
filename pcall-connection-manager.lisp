;; note: that in order to run this you need to add &ALLOW-OTHER-KEYS to the end of
;; HUNCHENTOOT:START-SERVER the function
;;
;;    (require :pcall-connection-manager)
;;    (setf (pcall:thread-pool-size) 20) ; remember to run this
;;    (hunchentoot:start-server :connection-manager-class 'pcall-connection-manager:pcall-connection-manager)
;;

(defpackage :pcall-connection-manager
  (:use :cl :hunchentoot)
  (:export #:pcall-connection-manager))

(in-package :pcall-connection-manager)

(defclass pcall-connection-manager (hunchentoot::connection-manager)
  ((acceptor-process :accessor acceptor-process
		     :documentation "Process that accepts incoming
                                     connections and creates a task that will proccess
                                     the connection"))
  (:documentation "Connection manager that starts a new thread for
listening to incoming requests. This thread uses PCALL:PCALL to create tasks
to process each connection that are run by by thread pool"))

(defmethod hunchentoot::execute-acceptor ((manager pcall-connection-manager))
  (setf (acceptor-process manager)
        (bt:make-thread (lambda () (hunchentoot::accept-connections (hunchentoot::server manager)))
                        :name (format nil
				      "Hunchentoot acceptor \(~A:~A)"
                                      (or (server-address (hunchentoot::server manager))
					  "*")
                                      (server-port (hunchentoot::server manager))))))

(defmethod hunchentoot::shutdown ((manager pcall-connection-manager))
  (loop
     while (bt:thread-alive-p (acceptor-process manager))
     do (sleep 1)))

(defmethod hunchentoot::handle-incoming-connection ((manager pcall-connection-manager) socket)
  (pcall:pcall (lambda () (hunchentoot::process-connection (hunchentoot::server manager) socket))))
